
	#include <iostream>
	#include <string>
	#include <vector>
	#include "windows.h"
	#include <SFML/Graphics.hpp>

	#include "Global_Variables.h"
	#include "Creature.cpp"
	#include "Items.cpp"
	#include "Inventory.h"
	#include "Interface_elements.cpp"
	using namespace std;
	using namespace sf;
class Programme{
    public:
	static RenderWindow window;

	static Clock Main_clock;
	static Event event;
	static Font General_font;
	static float Timer;

	static bool Sleeping;
	static bool Chilling;



	static Creature creature_;


	static bool GameStarted;
	static Sprite Artyom_sprite;
	static Texture Artyom_normal;//CREATURE
	static Texture Artyom_angry;


	static Button PlayButton;
	static Menu menu;

	static Bar hunger;
	static Bar hygiene;
	static Bar leisure;
	static Bar energy;


	static Food Shipovnik;
	static Food Tea;
	static Food Conserva;
	static ItemBar<Food> FoodBar;

	static void StartGame(){
		GameStarted = true;
	}

	class Interlayer {
	public:
		static Creature* creature ;
		static Background* bg ;
		static void changeRight(){
			bg->Change_bg(+1);
		}

		static void changeLeft(){
			bg->Change_bg(-1);
		}

		static void ChangeHunger( double value){
			creature->setHunger(creature->getHunger()+value);
			hunger.setBarValue(creature->getHunger());
		}

		static void ChangeHygiene( double value){
			creature->setHygiene(creature->getHygiene()+value);
			hygiene.setBarValue(creature->getHygiene());
		}

		static void ChangeEnergy( double value){
			creature->setEnergy(creature->getEnergy()+value);
			energy.setBarValue(creature->getEnergy());
		}

		static void ChangeLeisure( double value){
			creature->setLeisure(creature->getLeisure()+value);
			leisure.setBarValue(creature->getLeisure());
		}

		static void DecreaseAll(/*double hu, double hy, double en, double le*/){
			ChangeHunger(-0.5/Timer);
			ChangeHygiene(-0.25/Timer);
			ChangeEnergy(-0.25/Timer);
			ChangeLeisure(-1/Timer);

		   /* if (creature->getEnergy()>10 && creature->getHunger()>10 && creature->getHygiene()>10 && creature->getLeisure()>10) Artyom_sprite.setTexture(Artyom_normal);
			else Artyom_sprite.setTexture(Artyom_normal);*/
		}

		static void Eat(){
			auto val = FoodBar.getCurrentItem().getRestoreValue();
			ChangeHunger(val);
		}

		static void Sleep(){
			Sleeping = true;
			//ChangeEnergy(100);
		}

		static void SleepAnim(){
			static double T = 0;
			T += Timer;
			if (T>55000*Timer || creature->getEnergy()>99) {
				T = 0;
				Sleeping = false;
				return;
			}
			Texture t;
			t.loadFromFile(SLEEP_Z,IntRect(0,0,48,48));
			Sprite Z1;
			Z1.setTexture(t);
			int X=470,Y=250;
			//window.clear();
			for(int i = 0; i < 3; i++){
				Z1.setPosition(X,Y);
				window.draw(Z1);
				Z1.move(+65,-20);
				window.draw(Z1);
				Z1.move(+140,-68);
				window.draw(Z1);
			}
			ChangeEnergy(+2.25/Timer);
		}

		static void ChillAnim(){
			static double T = 0;
			T += Timer;
			if (T>55000*Timer || creature->getLeisure()>99) {
				T = 0;
				Chilling = false;
				return;
			}
			Texture t;
			t.loadFromFile(SMILE,IntRect(0,0,48,48));
			Sprite Z1;
			Z1.setTexture(t);
			int X=470,Y=250;
			//window.clear();
			for(int i = 0; i < 3; i++){
				Z1.setPosition(X,Y);
				window.draw(Z1);
				Z1.move(+65,-20);
				window.draw(Z1);
				Z1.move(+110,-68);
				window.draw(Z1);
			}
			ChangeEnergy(-2.25/Timer);
			ChangeHygiene(-2.55/Timer);
			ChangeLeisure(3/Timer);
		}

		static void Wash(){
			ChangeHygiene(100);
		}

		static void Play(){
			Chilling = true;
		}
	};


	static Button eat;
	static Button sleep_;
	static Button play;
	static Button wash;

	static Background BG;


	static Button rightButton;
	static Button leftButton;
};

Font Programme::General_font = Font();
Texture  Programme::Artyom_normal = Texture();
Texture Programme::Artyom_angry = Texture();
Sprite  Programme::Artyom_sprite = Sprite();

Clock Programme::Main_clock = Clock();
Event Programme::event = Event();
float Programme::Timer = 0;

RenderWindow Programme::window(VideoMode(800, 600), "Tamagochi");
Creature Programme::creature_ = Creature("Artyom");
bool Programme::GameStarted = false;
bool Programme::Sleeping = false;
bool Programme::Chilling = false;
Button Programme::PlayButton(Programme::window,Programme::event,Vector2f(376,376),PLAY,PLAY_CLICKED,Programme::StartGame);
Menu Programme::menu(Programme::window,&Programme::PlayButton,MENU);

Bar Programme::hunger(Vector2f(50,500),window,"Hunger",General_font);
Bar Programme::hygiene(Vector2f(650,500),window,"Hygiene",General_font);
Bar Programme::leisure(Vector2f(650,570),window,"Leisure",General_font);
Bar Programme::energy(Vector2f(50,570),window,"Energy",General_font);

Food Programme::Shipovnik("Shipovnik",50,13);
Food Programme::Tea("Tea with shipovnik",70,1);
Food Programme::Conserva("Conserva",70,3);
ItemBar<Food> Programme::FoodBar(window,event,Vector2f(150,500),Vector2f(48,48),Shipovnik,SHIPOVNIK,SHIPOVNIK_SELECTED,General_font);

Button Programme::eat(window,event,Vector2f(50,100),EAT,EAT_CLICKED,Interlayer::Eat);
Button Programme::sleep_(window,event,Vector2f(50,100), SLEEP,SLEEP_CLICKED,Interlayer::Sleep);
Button Programme::play(window,event,Vector2f(50,100),LEISURE,LEISURE_CLICKED,Interlayer::Play);
Button Programme::wash(window,event,Vector2f(50,100), WASH,WASH_CLICKED,Interlayer::Wash);

Background Programme::BG(kochalka,KOCHALKA_NAME,window,&play);

Button Programme::rightButton(Programme::window,Programme::event,Vector2f(720,260),TURN_RIGHT,TURN_RIGHT_CLICKED,Programme::Interlayer::changeRight);
Button Programme::leftButton(Programme::window,Programme::event,Vector2f(20,260), TURN_LEFT,TURN_LEFT_CLICKED,Programme::Interlayer::changeLeft);



Creature* Programme::Interlayer::creature = &Programme::creature_;
Background* Programme::Interlayer::bg = &Programme::BG;

using namespace sf;
int main()
{
    Programme::General_font.loadFromFile(BEER_MONEY_FONT);
    Programme::Artyom_normal.loadFromFile(ARTYOM_NORMAL);
    Programme::Artyom_angry.loadFromFile(ARTYOM_ANGRY);
    Programme::Artyom_sprite.setTexture(Programme::Artyom_normal);



    Programme::BG.addTexture(bathroom,BATHROOM_NAME,&Programme::wash);
    Programme::BG.addTexture(kitchen,KITCHEN_NAME,&Programme::eat);
    Programme::BG.addTexture(bedroom,BEDROOM_NAME,&Programme::sleep_);

    Programme::FoodBar.AddItem(Programme::Tea,TEA,TEA_SELECTED);
    Programme::FoodBar.AddItem(Programme::Conserva,CONSERVA,CONSERVA_SELECTED);



    while (Programme::window.isOpen())
    {
        Programme::Timer = Programme::Main_clock.getElapsedTime().asMicroseconds(); //���� ��������� ���� � �������������
        Programme::Main_clock.restart(); //������������� ����
        Programme::Timer/=100;

        while (Programme::window.pollEvent(Programme::event))
        {
            if (Programme::event.type == Event::Closed) Programme::window.close();
            else if (Programme::event.type == Event::MouseButtonPressed) {
                Programme::rightButton.checkClick();
                Programme::leftButton.checkClick();
                Programme::BG.getCurrentButton()->checkClick();
                Programme::menu.getCurrentButton()->checkClick();
            }
            if (Programme::BG.getCurrentName()==KITCHEN_NAME) Programme::FoodBar.checkSelection();
        }

        if (!Programme::GameStarted) {
            Programme::menu.draw();
            Programme::window.display();
            continue;
        }

        Programme::window.clear();

        Programme::Interlayer::DecreaseAll();
        Programme::BG.draw();
        Programme::window.draw(Programme::Artyom_sprite);
        Programme::hunger.draw();
        Programme::energy.draw();
        Programme::leisure.draw();
        Programme::hygiene.draw();
        Programme::rightButton.draw();
        Programme::leftButton.draw();
        if (Programme::BG.getCurrentName()==KITCHEN_NAME) Programme::FoodBar.draw();
        if (Programme::Sleeping) Programme::Interlayer::SleepAnim();
        if (Programme::Chilling) Programme::Interlayer::ChillAnim();

        Programme::window.display();
    }
    return 0;
}

//int main(){return 0;}
