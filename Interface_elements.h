#ifndef INTERFACE_ELEMENTS_INCLUDED
#define INTERFACE_ELEMENTS_INCLUDED
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Global_Variables.h"
using namespace sf;
using namespace std;

class Button{
public:
    RenderWindow& window;
    Texture button_texture,button_texture_clicked;
    Sprite button_sprite;
    Vector2f button_coords;
    int b_sizeX=48, b_sizeY=48;
    Event& event;
    void (*func)();
    bool clicked = false;

    Button(RenderWindow& , Event& ,Vector2f ,string ,string ,void (*)());

    void checkClick();

     void draw();
};

class Background{
public:
    RenderWindow& window;
    vector<Texture> bg;
    vector<string> bg_name;
    int current_bg=0;
    Sprite bg_sprite;
    vector<Button*> btns;

    Background(string ,string , RenderWindow& , Button* );

    void addTexture(string , string , Button* );

    void Change_bg(int );

    string getCurrentName();

    Button* getCurrentButton();

    void draw();
};


template<typename item>
class ItemBar{
public:
    RenderWindow& window;
    Event& event;
    Font& font;
    vector<item> items;
    vector<Sprite> sprites;
    vector<Texture> icons;
    vector<Texture> icons_selected;
    vector<Text> counts;
    int current_pos = 0;
    Vector2f Position;
    Vector2f iconSize;

    ItemBar(RenderWindow& _window,
            Event& _event,
            Vector2f _pos,
            Vector2f _iconSize,
            item i,
            string path_icon,
            string path_icon_selected,
            Font& _font);
    void checkSelection();
    item getCurrentItem();
    void draw();
    void SelectItem(int pos);
    void AddItem(item i,string path_icon, string path_icon_selected);

};



class Bar{
public:

    Font font;//�����
    Text text;         // ("", font, 20);//������� ������ �����. ���������� � ������ ����� ������, �����, ������ ������(� ��������);//��� ������ ����� (�� ������)
    RenderWindow& window;
    RectangleShape bar;
    Texture empty_bar_texture;
    Sprite empty_bar_sprite;
    Vector2f coords;
    const int maxSizeX=100, sizeY=20;
    int currentX;

    Bar(Vector2f coords,RenderWindow& _window, string barName);

    void changeBarValue(int Value=1);

    void setBarValue(int Value = 100);

    void draw();
};

#endif // INTERFACE_ELEMENTS_INCLUDED
