#include <iostream>
#include <string>
#include <vector>
using namespace std;
class Creature{
private:
    static const int parameter_max_value = 100;

    string name;
    double hunger;
    double hygiene;
    double energy;
    double leisure;
public:
    Creature(string _name,
            double _hunger,
            double _hygiene,
            double _energy,
            double _leisure)
    {name=_name; hunger=_hunger;hygiene=_hygiene;energy=_energy;leisure=_leisure;}
    Creature(string _name){
        name = _name;
        hunger = parameter_max_value;
        hygiene = parameter_max_value;
        energy = parameter_max_value;
        leisure = parameter_max_value;
    }

    ~Creature(){delete(&name);}

    string getName(){return name;}
    double getHunger(){return hunger;}
    double getHygiene(){return hygiene;}
    double getEnergy(){return energy;}
    double getLeisure(){return leisure;}

    void setName(string _name){ name = _name; }
    void setHunger(double _hunger){
        if (_hunger>parameter_max_value) hunger = parameter_max_value;
        else if (_hunger<0) hunger = 0; else hunger = _hunger;
    }

    void setHygiene(double _hygiene){
        if (_hygiene>parameter_max_value) hygiene = parameter_max_value;
        else if (_hygiene<0) hygiene = 0; else hygiene = _hygiene;
    }

    void setEnergy(double _energy){
        if (_energy>parameter_max_value) energy = parameter_max_value;
        else if (_energy<0) energy = 0; else energy = _energy;
    }

    void setLeisure(double _leisure){
        if (_leisure>parameter_max_value) leisure = parameter_max_value;
        else if (_leisure<0) leisure = 0; else leisure = _leisure;
    }

};
