#ifndef ITEMS_INCLUDE
#define ITEMS_INCLUDE
using namespace std;

class Item{
protected:
    int count=1;
    Item(int _count = 1);
public:
    int getCount();
};

class Consumable{
    public:
    virtual bool Consume() = 0;
};


class Food: public Item, public Consumable{
private:
    string name;
    int saturation;

public:
    Food(string _name, int _saturation, int count=1);
    ~Food();

    string getName();
    int getSaturation();

    int getCount();
    virtual bool Consume() ;
};

/*class Accessories{
private:
    string name;
    int slot; //���� ����������� ������� (������, ����, �����-������ ���� � ��)

public:
    Accessories(string _name, int _slot){ name=_name; slot=_slot;}
    ~Accessories(){delete(&name);}

    string getName(){return name;}
    int getSlot(){return slot;}
};*/

class Hygiene{
private:
    string name;
    int efficiency;

public:
    Hygiene(string _name, int _efficiency);
    ~Hygiene();

    string getName();
    int getSlot();
};

#endif // ITEMS_INCLUDE
