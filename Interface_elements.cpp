#include <iostream>
#include <string>
#include <vector>
#include "windows.h"
#include <SFML/Graphics.hpp>
#include "Global_Variables.h"
using namespace sf;
using namespace std;


class Button{
public:
    RenderWindow& window;
    Texture button_texture,button_texture_clicked;
    Sprite button_sprite;
    Vector2f button_coords;
    int b_sizeX=48, b_sizeY=48;
    Event& event;
    void (*func)();
    bool clicked = false;
//window � event - ������
    Button(RenderWindow& _window, Event& _event,Vector2f b_coords,string texture,string texture_cl,void (*_func)()=0):window(_window),event(_event){
        button_coords=b_coords;
        button_texture.loadFromFile(texture);
        button_texture_clicked.loadFromFile(texture_cl);
        button_sprite.setPosition(button_coords);
        button_sprite.setTexture(button_texture);
        func=_func;
    }

    void checkClick(){
        if (IntRect(button_coords.x,button_coords.y,b_sizeX,b_sizeY).contains(Mouse::getPosition(window))) {
            if (event.mouseButton.button == Mouse::Left){
                button_sprite.setTexture(button_texture_clicked);
                if (func) {func();}
                return;
            }
        }
        button_sprite.setTexture(button_texture);
    }

     void draw(){
        window.draw(button_sprite);

        if (event.type == Event::MouseButtonReleased)
            button_sprite.setTexture(button_texture);

    }
};

class Menu {
    public:
    RenderWindow& window;
    Button* button;
    Texture texture;
    Sprite background;

    Menu(RenderWindow& _window, Button* _button, string path):window(_window){
        button = _button;
        background.setPosition(0,0);
        auto t = new Texture();
        t->loadFromFile(path);
        background.setTexture(*t);
    }
    Button* getCurrentButton() {
        return button;
    }

    void draw(){
        window.draw(background);
        button->draw();
    }


};



class Background{
public:
    RenderWindow& window;
    vector<Texture> bg;
    vector<string> bg_name;
    int current_bg=0;
    Sprite bg_sprite;
    vector<Button*> btns;

    Background(string path,string name, RenderWindow& _window, Button* button):window(_window){
        bg_sprite.setPosition(0,0);
        addTexture(path,name,button);
        bg_sprite.setTexture(bg[0]);
    }

    void addTexture(string path, string name, Button* button){
        auto t = new Texture();
        t->loadFromFile(path);
        bg.push_back(*t);
        bg_name.push_back(name);
        btns.push_back(button);
    }

    void Change_bg(int next_bg){
        if (next_bg>=0) {
            if (current_bg>=bg.size()-1) current_bg=0;
            else current_bg++;
        }
        else {
            if (current_bg<=0) current_bg=bg.size()-1;
            else current_bg--;
        }
        bg_sprite.setTexture(bg[current_bg]);
    }

    string getCurrentName(){
        return bg_name[current_bg];
    }

    Button* getCurrentButton() {
        return btns[current_bg];
    }

    void draw(){
        window.draw(bg_sprite);
        btns[current_bg]->draw();
    }
};


template<typename item>
class ItemBar{
public:
    RenderWindow& window;
    Event& event;
    Font& font;
    vector<item> items;
    vector<Sprite> sprites;
    vector<Texture> icons;
    vector<Texture> icons_selected;
    vector<Text> counts;
    int current_pos = 0;
    Vector2f Position;
    Vector2f iconSize;

    ItemBar(RenderWindow& _window,
            Event& _event,
            Vector2f _pos,
            Vector2f _iconSize,
            item i,
            string path_icon,
            string path_icon_selected,
            Font& _font)

        :window(_window),event(_event),font(_font){

        Position = _pos;
        iconSize = _iconSize;
        AddItem(i,path_icon,path_icon_selected);
        //SelectItem(0);
        sprites[current_pos].setTexture(icons_selected[current_pos]);
    }


    void checkSelection(){
        for (int i = 0; i<items.size(); i++)
            if ( IntRect(Position.x + iconSize.x * i, Position.y, iconSize.x, iconSize.y).contains(Mouse::getPosition(window) ) ) {
                if (event.mouseButton.button == Mouse::Left){
                    SelectItem(i);
                    return;
                }
            }
    }

    item getCurrentItem(){
        return items[current_pos];
    }


    void draw(){

        for (int i = 0; i<items.size(); i++) {
            window.draw(sprites[i]);
            counts[i].setString("+" + to_string( (int) items[i].getRestoreValue()));
            window.draw(counts[i]);
        }

    }

    void SelectItem(int pos){
        sprites[current_pos].setTexture(icons[current_pos]);
        current_pos = pos;
        sprites[current_pos].setTexture(icons_selected[current_pos]);
    };

    void AddItem(item i,string path_icon, string path_icon_selected){
        //������� ������ �������� ��� �������� �������� ����������� ������
        auto t1 = new Texture();
        t1->loadFromFile(path_icon);

        //������� ������ �������� ��� �������� �������� ��������� ������
        auto t2 = new Texture();
        t2->loadFromFile(path_icon_selected);


        //������� ������ ������ ��� �������� �������
        auto s = new Sprite(*t1,IntRect(0,0,iconSize.x,iconSize.y));
        Vector2f SpritePos(Position.x + iconSize.x * items.size(), Position.y);

        s->setPosition(SpritePos);


        auto cnt = new Text("",font,20);
        Vector2f TextPos(SpritePos.x+27,SpritePos.y+29);
        cnt->setColor(Color(255,0,246));
        cnt->setPosition(TextPos);

        items.push_back(i);
        icons.push_back(*t1);
        icons_selected.push_back(*t2);
        sprites.push_back(*s);
        counts.push_back(*cnt);
    }

};





class Bar{
public:

    Font& font;//�����
    Text text; //������� ������ �����.
    RenderWindow& window;
    RectangleShape bar;
    Texture empty_bar_texture;
    Sprite empty_bar_sprite;
    Vector2f coords;
    const int maxSizeX=100, sizeY=20;
    int currentX;

    Bar(Vector2f coords,RenderWindow& _window, string barName, Font& _font):window(_window),font(_font){
        //�������� ������ ������ ���� ������
        text=Text("", font, 20);//������� ������ �����. ���������� � ������ ����� ������, �����, ������ ������(� ��������);
        currentX=maxSizeX;
        this->coords = coords;
        empty_bar_texture.loadFromFile(EMPTY_BAR);
        empty_bar_sprite.setPosition(coords);
        empty_bar_sprite.setTexture(empty_bar_texture);
        bar.setPosition(coords);
        bar.setFillColor(Color(127,176,60));//��������� �������
        bar.setSize(Vector2f(currentX,sizeY));

        text.setColor(Color::White);
		text.setString(barName);//������ ������ ������
		text.setPosition(coords.x+25,coords.y-27);//������ ������� ������
    }

    void changeBarValue(int Value=1){
        if (currentX>=0&&currentX<=maxSizeX){
            currentX+=Value;
        }
        draw();
    //    printError();
    }

    void setBarValue(int Value = 100){
        if (Value > maxSizeX) currentX = maxSizeX; else if (Value<0) currentX = 0; else currentX = Value;
    }

    void draw(){
        bar.setSize(Vector2f(currentX,sizeY));
        window.draw(bar);
        window.draw(empty_bar_sprite);
        window.draw(text);
    }
};
