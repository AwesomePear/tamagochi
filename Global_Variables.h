#ifndef GLOBAL_VARIABLES_INCLUDED
#define GLOBAL_VARIABLES_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "windows.h"
#include <SFML/Graphics.hpp>
using namespace std;



const string kochalka = "Images/kochalka.png";
const string kitchen = "Images/kitchen.png";
const string bedroom = "Images/bedroom.png";
const string bathroom = "Images/bathroom.png";

const string ARTYOM_NORMAL = "Images/Artyom_0_normal.png";
const string ARTYOM_ANGRY = "Images/Artyom_0_normal_angry.png";

const string BEER_MONEY_FONT = "Images/beer_money.ttf";
const string EMPTY_BAR = "Images/bar_empty.png";
const string TURN_RIGHT =  "Images/turn_right.png";
const string TURN_RIGHT_CLICKED = "Images/turn_right_clicked.png";
const string TURN_LEFT = "Images/turn_left.png";
const string TURN_LEFT_CLICKED = "Images/turn_left_clicked.png";

const string EAT = "Images/eat.png";
const string EAT_CLICKED = "Images/eat_clicked.png";
const string SLEEP = "Images/sleep.png";
const string SLEEP_CLICKED = "Images/sleep_clicked.png";
const string LEISURE = "Images/leisure.png";
const string LEISURE_CLICKED = "Images/leisure_clicked.png";
const string WASH = "Images/wash.png";
const string WASH_CLICKED = "Images/wash_clicked.png";

const string SLEEP_Z = "Images/Sleep_Z.png";
const string SMILE = "Images/Smile.png";

const string PLAY = "Images/Play.png";
const string PLAY_CLICKED = "Images/Play_clicked.png";

const string KOCHALKA_NAME = "kochalka";
const string KITCHEN_NAME = "kitchen";
const string BEDROOM_NAME = "bedroom";
const string BATHROOM_NAME = "bathroom";

const string MENU = "Images/Menu.png";

const string SHIPOVNIK = "Images/Shipovnik_icon.png";
const string SHIPOVNIK_SELECTED = "Images/Shipovnik_icon_selected.png";
const string CONSERVA = "Images/Conserva_icon.png";
const string CONSERVA_SELECTED = "Images/Conserva_icon_selected.png";
const string TEA = "Images/Tea_icon.png";
const string TEA_SELECTED = "Images/Tea_icon_selected.png";

//const string  = ;


#endif // GLOBAL_VARIABLES_INCLUDED
