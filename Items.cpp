#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Item{
protected:
    int count;
    Item(int _count = 1){count = _count;}
public:
    int getCount() {return count;}
};

class Consumable{
    public:
    virtual bool Consume() = 0;

    virtual double getRestoreValue() = 0;
};


class Food: public Item, public Consumable{
private:
    string name;
    int saturation;

public:
    Food(string _name, int _saturation, int count=1):Item(count){ name=_name; saturation=_saturation;}
    ~Food(){delete(&name);}

    string getName(){return name;}
    virtual double getRestoreValue(){return saturation;}

    int getCount() {return count;}
    virtual bool Consume() {
        if (count<=0) return false;

        count--;
        return true;
    }
};

/*class Accessories{
private:
    string name;
    int slot; //���� ����������� ������� (������, ����, �����-������ ���� � ��)

public:
    Accessories(string _name, int _slot){ name=_name; slot=_slot;}
    ~Accessories(){delete(&name);}

    string getName(){return name;}
    int getSlot(){return slot;}
};*/

class Hygiene{
private:
    string name;
    int efficiency;

public:
    Hygiene(string _name, int _efficiency){ name=_name; efficiency=_efficiency;}
    ~Hygiene(){delete(&name);}

    string getName(){return name;}
    int getSlot(){return efficiency;}
};
