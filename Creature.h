#ifndef CREATURE_INCLUDE
#define CREATURE_INCLUDE
using namespace std;

class Creature{
private:
    static const int parameter_max_value = 100;

    string name;
    double hunger;
    double hygiene;
    double energy;
    double leisure;
public:
Creature(string _name,
            double _hunger,
            double _hygiene,
            double _energy,
            double _leisure);
    Creature(string );

    ~Creature();

    string getName();
    double getHunger();
    double getHygiene();
    double getEnergy();
    double getLeisure();

    setName(string _name);
    setHunger(double _hunger);
    setHygiene(double _hygiene);
    setEnergy(double _energy);
    setLeisure(double _leisure);

};

#endif // CREATURE_INCLUDE
